package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {
	
	private DivvyTripsManager mundo;

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		String stationsFile = "./data/Divvy_Stations_2017_Q3Q4.csv";
		manager.loadStations(stationsFile);
		
	}
	
	public static void loadTrips() {
		String tripsFile = "./data/Divvy_Trips_2017_Q4.csv";
		manager.loadTrips(tripsFile);
		
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
	
		return manager.getTripsOfGender(gender);
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}

