//package opencsv;
//import src/main/java.com.opencsv.CSVReader.java
package model.logic;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;


import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

//import opencsv.java.com.opencsv.*;
//import opencsv.java.com.opencsv.bean.customconverter.*;
//import opencsv.java.com.opencsv.CSVReader;
////import com.opencsv.CSVReader;
//
//import java.beans.beancontext.*;
//import java.beans.*;

public class DivvyTripsManager implements IDivvyTripsManager {

	//atributos

	private DoubleLinkedList<VOTrip> tripsList;
	private DoubleLinkedList<VOByke> stationsList;

	//m�todos	


	public void loadStations (String stationsFile)  {


		try {
			//CSVReader reader = new CSVReader(new FileReader(stationsFile));

			BufferedReader bf = new BufferedReader(new FileReader(stationsFile));
			stationsList = new DoubleLinkedList<VOByke>();

			int id;
			String name;
			String city;
			String latitude;
			String longitude;
			int dpCapacity;
			String date;

			String datos = bf.readLine();
			char quoteChar = '"';
			
			datos = bf.readLine();

			while(datos != null){

				String[] stations = datos.split(",");

	
				id = Integer.parseInt(stations[0]);
				name = stations[1];					
				city = stations[2];					
				latitude = stations[3];					
				longitude = stations[4];					
				dpCapacity = Integer.parseInt(stations[5]);
				date = stations[6];			
					

				datos = bf.readLine();

				stationsList.addAtEnd(new VOByke(id,name,city,latitude,longitude,dpCapacity,date)); //crea el objeto
			}
			
			bf.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void loadTrips (String tripsFile) {		

		try{
			BufferedReader bf = new BufferedReader(new FileReader(tripsFile));
			tripsList = new DoubleLinkedList<VOTrip>();

			int tripId;
			String startTime;
			String endTime;
			int bikeId;
			String tripDuration;
			int fromStationId;
			String fromStationName;
			int toStationId;
			String toStationName;
			String userType;
			String gender;
			int birthYear;

			String datos = bf.readLine();
			datos = bf.readLine();		
			


			while(datos != null){

				String[] trips = datos.split(",");
				System.out.println(trips.length);
				
				
			
				//opencsv
				
				tripId = Integer.parseInt(trips[0].replace("\"", ""));				
				startTime = trips[1];					
				endTime = trips[2];									
				bikeId = Integer.parseInt(trips[3].replace("\"", ""));									
				tripDuration = trips[4];								
				fromStationId = Integer.parseInt(trips[5].replace("\"", ""));					
				fromStationName = trips[6];									
				toStationId = Integer.parseInt(trips[7].replace("\"", ""));					
				toStationName = trips[8];						
				userType = trips[9];
				
				if(trips.length < 12){
					gender = "vac�o";
					birthYear = -1;
				}
				else{
					gender = trips[10]; 
					birthYear = Integer.parseInt(trips[11].replace("\"", ""));
				}
					



				tripsList.addAtEnd(new VOTrip(tripId,startTime,endTime,bikeId,tripDuration,
						fromStationId,fromStationName,
						toStationId,toStationName,userType,gender,birthYear));	

				System.out.println(datos);

				datos = bf.readLine();
				
			}
			bf.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {

//		if(tripsList == null)
//			loadTrips(tripsFile);

		DoubleLinkedList tripsOfGender = new DoubleLinkedList();

		
		Node<VOTrip> actual = tripsList.getCurrentElement();
		
		while(actual.getNext() != null){
			
			if(actual.getItem().getGender().equals(gender)){
				
				tripsOfGender.addAtEnd(actual);				
			}
			actual = actual.getNext();
			
		}

		return tripsOfGender;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {

		//String stationsFile = "./data/Divvy_Stations_2017_Q4.csv";

//		if(tripsList == null)
//			loadTrips(tripsFile);

		DoubleLinkedList tripsToStation = new DoubleLinkedList();
		Node<VOTrip> actual = tripsList.getCurrentElement();
		
		while(actual.getNext() != null){
			
			if(actual.getItem().getToStationId() == stationID){
				tripsToStation.addAtEnd(actual);				
			}
			actual = actual.getNext();			
		}

		return tripsToStation;
	}	


}
