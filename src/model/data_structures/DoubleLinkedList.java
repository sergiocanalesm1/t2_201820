package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T> implements DoublyLinkedList<T>{

	//atributos

	private Node<T> list;
	private int listSize;
	//constructores

	public DoubleLinkedList()
	{
		list = null;
	}
	public DoubleLinkedList(T item )
	{
		list = new Node<T>(item);
		listSize++;
		

	}

	//m�todos
	

	public void addFirst(T item){

		Node<T> first = new Node<T>(item);

		first.setNextNode(list);
		//list.setPrevNode(first);
		list = first;
		listSize++;	
	}


	public void addAppend(T item, int pos) // no sirve para el primero
	{
		Node<T> newN = new Node<T>(item);		
		Node<T> actual = list;
		if(list == null)
		{
			list = newN;
		}

		for(int i = 1 ; i < pos ; i++){
			if(i == (pos - 1 )){
				newN.setNextNode(actual.getNext().getNext());
				actual.setNextNode(newN);
				newN.setPrevNode(actual);
				listSize++;
			}
			actual = actual.getNext();

		}

	}
	@Override
	public Iterator<T> iterator(){
		return null;
	}

	@Override
	public void addAtEnd(T item) {

		Node<T> actual = list;
		if(actual == null){
			addFirst(item);
		}
		else{

			while(actual.getNext() !=null){
				actual = actual.getNext();
			}
			actual.setNextNode(new Node<T>(item));
			listSize++;
		}

	}

	@Override
	public Node getElement(int pos) {
		
		Node<T> actual = list;
		int i = 0;
		while(pos != i)
		{
			i++;
			actual = actual.getNext();
		}
		return actual;

	}
	@Override
	public Node<T> getCurrentElement() {
		return list;
	}
	@Override
	public void delete() {
		list = null;
		listSize = 0;
	}

	@Override
	public Node next() {
		return list.getNext();//no entiendo para qu� sirven estos dos m�todos
		
	}
	@Override
	public Node previous() {
		return list.getPrev();
	}
	@Override
	public Integer getSize() {
		return listSize;
	}
	@Override
	public void deleteAtK(int pos) {
		Node<T> actual = list;
		int i = 0;
		while(i != (pos - 1)){
			actual = actual.getNext();
			i++;
		}
		actual.setNextNode(actual.getNext());
		listSize--;


	}



}
