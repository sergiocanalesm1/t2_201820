package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * addFirst, addAtEnd, AddAppend, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {
	
	public void addAppend(T item , int pos);	
	public void addFirst(T item);
	public Node getElement(int pos);
	public Node getCurrentElement();
	public void delete();
	public void deleteAtK(int pos);
	public Node next();
	public Node previous();
	Integer getSize();
	void addAtEnd(T item);

}
