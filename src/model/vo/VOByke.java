package model.vo;

/**
 * Representation of a byke object
 */
public class VOByke {
	
	//atributos
	
	private int id;
	private String name;
	private String city;
	private String latitude;
	private String longitude;
	private int dpCapacity;
	private String date;
	
	//constructor
	
	public VOByke(int id, String name , String city , String latitude ,
			String longitude , int dpCapacity , String date){
		
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpCapacity = dpCapacity;
		this.date = date;
	}

	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		return id;
	}	
	public String getLatitude(){
		return latitude;
	}
}
