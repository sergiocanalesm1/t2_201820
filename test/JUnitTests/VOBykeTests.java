package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.vo.VOByke;

public class VOBykeTests {
	
	
	private VOByke byke;

	public void setupScenario1(){
		
		String latitude = "183";
		byke = new VOByke(12,"","",latitude, "" ,0,"");
	}
	
	@Test
	public void test(){
		
		setupScenario1();
		assertEquals("el id no corresponde", 12, byke.id());
		assertEquals("latitude no concuerda" , "183" , byke.getLatitude());
	}


}
