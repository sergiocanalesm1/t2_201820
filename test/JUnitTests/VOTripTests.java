package JUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.vo.VOTrip;

public class VOTripTests {

	private VOTrip trip;
	
	private void setupScenario1(){
		
		String tripDuration = "01:01:01";
		trip = new VOTrip(12, null, null, 12, tripDuration, 0, null, 0, null, null, "Male", 1998);
	}
	
	
	public void testTrips(){
		
		setupScenario1();	
		
		assertEquals("el id no corresponde" , 12 , trip.id());
		assertEquals("el tiempo en segundos no es correcto", 3661 , trip.getTripSeconds());
		assertEquals("el g�nero no corresponde", "Male" , trip.getGender());
		
	}
}
